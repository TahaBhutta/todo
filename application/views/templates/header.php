<html>
  <head>
    <title>Todo List</title>
    <link rel="stylesheet" href="https://bootswatch.com/flatly/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <script src="http://cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
  </head>
  <body>
  <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo base_url(); ?>">Todoist</a>
        </div>
        <div id="navbar">
          <ul class="nav navbar-nav">
          <?php 
              if ($this->session->userdata('logged_in')) {
                echo "<li><a href='". base_url()."lists/index'>Lists</a></li>";
                echo "<li><a href='". base_url()."tasks/index'>Tasks</a></li>";
              }
          ?>
          </ul>
          <ul class="nav navbar-nav navbar-right">

            <?php 
              if (!$this->session->userdata('logged_in')) {
                echo "<li><a href='". base_url()."users/login'>Login</a></li>";
                echo "<li><a href='". base_url()."users/register'>Register</a></li>";
              } else{
                echo "<li><a href='". base_url()."lists/create'>Create List</a></li>";
                echo "<li><a href='". base_url()."tasks/create'>Create Task</a></li>";
                echo "<li><a href='". base_url()."users/logout'>Logout</a></li>";
              }
              ?>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container">
      <!-- Flash messages -->

      <?php if($this->session->flashdata('flash_success')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('flash_success').'</p>'; ?>
      <?php endif; ?>

      <?php if($this->session->flashdata('flash_failure')): ?>
        <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('flash_failure').'</p>'; ?>
      <?php endif; ?>


      <?php if($this->session->flashdata('flash_warning')): ?>
        <?php echo '<p class="alert alert-warning">'.$this->session->flashdata('flash_warning').'</p>'; ?>
      <?php endif; ?>
