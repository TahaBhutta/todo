
<?php echo validation_errors(); ?>
<?php echo form_open('lists/create'); ?>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h3 class="text-center"><?php echo isset($page_title) ? $page_title : ''; ?></h3>

			<div class="form-group">
				<?php echo form_label('Title', 'title'); ?>
				<?php $attr = array( 'type' => 'text', 'name' => 'title', 'value' => set_value('title'), 'class' => 'form-control', 'placeholder' => 'List Title', 'required' => 'required' ); 
			echo form_input($attr); ?>
			</div>

			<div class="form-group">
				<?php echo form_label('Description', 'desc'); ?>
				<?php echo form_textarea('desc', set_value('desc'), ['class' => 'form-control']); ?>
			</div>

			<button type="submit" class="btn btn-primary btn-block">Save</button>

		</div>
	</div>
<?php form_close(); ?>
