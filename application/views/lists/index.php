<h3><?php echo isset($page_title) ? $page_title : ''; ?></h3>

<ul class="list-group">
	<?php foreach ($lists as $list): ?>	
		<li class="list-group-item">
			<a href="<?php echo site_url('/lists/tasks/').$list['id']; ?>" title=""><?php echo $list['title']; ?> </a>
		</li>
	<?php endforeach; ?>
</ul>