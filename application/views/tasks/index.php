<h3><?php echo isset($page_title) ? $page_title : ''; ?></h3>

<?php $sr = 1; ?>

<?php if (isset($list)): ?>
  <div class="well">
<?php echo validation_errors(); ?>

<?php echo form_open('tasks/create',['class'=>'form-inline']); ?>

      <h4 class="text-left"><span class="glyphicon glyphicon-plus"></span>Add New Task</h4>

      <div class="form-group">
       
        <?php $attr = array( 'type' => 'text', 'name' => 'title', 'value' => set_value('title'), 'class' => 'form-control', 'placeholder' => 'Title', 'required' => 'required' ); 
      echo form_input($attr); ?>
      </div>

      <div class="form-group">
        <?php $attr = array( 'type' => 'text', 'name' => 'desc', 'value' => set_value('desc'), 'class' => 'form-control', 'placeholder' => 'Description (Optional)' ); 
      echo form_input($attr); ?>
      </div>
      
      <div class="form-group">
        <input type="datetime-local" name="due_date" class="form-control" min="<?php echo time(); ?>">
      </div>

      <?php echo form_hidden('list_id', $list['id']); ?>

      <button type="submit" class="btn btn-primary">Add</button>

<?php echo form_close(); ?>
</div>
<?php endif; ?>

<div class="table-responsive">
  
  <table class="table table-striped table-hover">
    <thead>
      <tr class="success"> 
        <th>SR#</th>
        <th>Title</th>
        <th>Created</th>
        <th>Due Date</th>
        <th>Completed</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
     
      <?php foreach ($tasks as $task): ?>
      	
        <tr>
      		<td><?php echo($sr) ?></td>
      		<td><?php echo($task['title']) ?></td>
      		<td><?php echo date('M j, Y g:i A', strtotime($task['created_at'])); ?></td>
      		<td><?php echo (strtotime($task['due_date']) < 0) ? '-': $task['due_date']; ?></td>
      		<td><?php echo($task['completed'] === '1') ? 'Yes':'No' ?></td>
          <td> 
            <a href="#" title="Task Completed" ><span class="glyphicon glyphicon-thumbs-down"></span></a> |
            <a href="#" title="Task Completed" ><span class="glyphicon glyphicon-pencil text-warning"></span></a> | 
            <a href="#" title="Task Completed" ><span class="glyphicon glyphicon glyphicon-remove text-danger"></span></a>  
          </td>
      	</tr>

      <?php $sr++; endforeach; ?>
     
    </tbody>
  </table>
</div>

