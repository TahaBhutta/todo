
<?php echo validation_errors(); ?>

<?php echo form_open('tasks/create'); ?>
	
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h3 class="text-center"><?php echo isset($page_title) ? $page_title : ''; ?></h3>

			<div class="form-group">
				<?php echo form_label('Title', 'title'); ?>
				<?php $attr = array( 'type' => 'text', 'name' => 'title', 'value' => set_value('title'), 'class' => 'form-control', 'placeholder' => 'Title', 'required' => 'required' ); 
			echo form_input($attr); ?>
			</div>

			<div class="form-group">
				<?php echo form_label('Description', 'desc'); ?>
				<?php echo form_textarea('desc', set_value('desc'), ['class' => 'form-control']); ?>
			</div>

			<div class="form-group">
				<?php echo form_label('List', 'list'); ?>
				<select name="list_id" class="form-control">
					<?php foreach ($lists as $list): ?>
						<option value="<?php echo $list['id'] ?>"><?php echo $list['title'] ?> </option>
					<?php endforeach; ?>
				</select>
			</div>
			
			<div class="form-group">
			 	<input type="datetime-local" name="due_date" class="form-control" min="<?php echo time(); ?>">
			</div>

			<button type="submit" class="btn btn-primary btn-block">Save</button>

		</div>
	</div>

<?php echo form_close(); ?>