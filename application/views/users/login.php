<h3><?php //echo isset($page_title) ? $page_title : ''; ?></h3>

<?php echo validation_errors(); ?>

<?php echo form_open('users/login'); ?>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h3 class="text-center"><?php echo isset($page_title) ? $page_title : ''; ?></h3>

			<div class="form-group">
				<?php echo form_label('Username', 'username'); ?>
				<?php $attr = array( 'type' => 'text', 'name' => 'username', 'value' => set_value('username'), 'class' => 'form-control', 'placeholder' => 'Username', 'required' => 'required' ); 
			echo form_input($attr); ?>
			</div>

			<div class="form-group">
				<?php echo form_label('Password', 'password'); ?>
				<?php $attr = array( 'type' => 'password', 'name' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'required' => 'required' ); 
			echo form_input($attr); ?>
			</div>

			<button type="submit" class="btn btn-primary btn-block">Login</button>

		</div>
	</div>
<?php form_close(); ?>