<h3><?php //echo isset($page_title) ? $page_title : ''; ?></h3>

<?php echo validation_errors(); ?>

<?php echo form_open_multipart('users/register'); ?>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h3 class="text-center"><?php echo isset($page_title) ? $page_title : ''; ?></h3>

			<div class="form-group">
				<?php echo form_label('Username', 'username'); ?>
				<?php $attr = array( 'type' => 'text', 'name' => 'username', 'value' => set_value('username'), 'class' => 'form-control', 'placeholder' => 'Username', 'required' => 'required' ); 
			echo form_input($attr); ?>
			</div>

			<div class="form-group">
				<?php echo form_label('Email', 'email'); ?>
				<?php $attr = array( 'type' => 'email', 'name' => 'email', 'value' => set_value('email'), 'class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required' ); 
			echo form_input($attr); ?>
			</div>

			<div class="form-group">
				<?php echo form_label('Password', 'pasword'); ?>
				<?php $attr = array( 'type' => 'password', 'name' => 'password', 'class' => 'form-control', 'placeholder' => 'Pasword', 'required' => 'required' ); 
			echo form_input($attr); ?>
			</div>

			<div class="form-group">
				<?php echo form_label('Confirm Password', 'confirm_password'); ?>
				<?php $attr = array( 'type' => 'password', 'name' => 'confirm_password', 'class' => 'form-control', 'placeholder' => 'Confirm Password', 'required' => 'required' ); 
			echo form_input($attr); ?>
			</div>


			<div class="form-group">
				<?php echo form_label('Profile Image', 'profile_img'); ?>
				<?php echo form_upload('profile_img'); ?>
			</div>
			
			<button type="submit" class="btn btn-primary btn-block">Submit</button>

		</div>
	</div>
<?php form_close(); ?>