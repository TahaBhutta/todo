<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	private $TABLE_NAME = 'users';
	private $PRI_INDEX = 'id';

    public function register($password,$post_image=nil)
    {
    	$data = array(
    		'username'    => $this->input->post('username'),
    		'email' 	  => $this->input->post('email'),
    		'password' 	  => $password,
    		'profile_img' => $post_image,
    		);

    	$this->db->set('created_at', 'CURRENT_TIMESTAMP()', FALSE);
    	$this->db->set('updated_at', 'CURRENT_TIMESTAMP()', FALSE);
    	return $this->db->insert($this->TABLE_NAME, $data);
    }

    public function login($username='')
    {
    	
    	$query = $this->db->get_where($this->TABLE_NAME, array('username'=>$username));    

    	if ($query->num_rows() == 1) {
			return $query->row_array();
		}else{
			return false;	
		}

    }


}
        

?>