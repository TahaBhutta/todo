<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class List_model extends CI_Model {
    
    private $TABLE_NAME = 'lists';
	private $PRI_INDEX = 'id';

	function __construct()
	{
		if(!$this->session->userdata('logged_in')){
        	redirect('users/login');
     	}
	}


	public function index()
	{

	}

	public function create()
	{

     	$data = array(
     		'user_id' => $this->session->userdata('user_id'),
     		'title' => $this->input->post('title'),
     		'desc' => $this->input->post('desc'),
     		'completed' => '0'
     	);

     	$this->db->set('created_at', 'CURRENT_TIMESTAMP()', FALSE);
    	$this->db->set('updated_at', 'CURRENT_TIMESTAMP()', FALSE);

     	return $this->db->insert($this->TABLE_NAME, $data);

	}

	public function get_lists()
	{	

     	$users_id = $this->session->userdata('user_id');

		$this->db->order_by('updated_at', 'desc');
		$query = $this->db->get_where($this->TABLE_NAME, array('user_id'=>$users_id));
		return $query->result_array();
	}

	public function get_list($list_id)
	{	
		$query = $this->db->get_where($this->TABLE_NAME, array('id'=>$list_id));
		return $query->row_array();
	}

}


?>