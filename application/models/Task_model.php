<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Task_model extends CI_Model {
    
    private $TABLE_NAME = 'tasks';
	private $PRI_INDEX = 'id';

	//id	list_id	user_id	title	desc	completed	due_date	creator_id

	public function index()
	{

	}

	public function create()
	{	

		$time = strtotime($this->input->post('due_date'));
		if ($time != false){
		  $new_date = date('Y-m-d H:i:s', $time);  
		}
		else{
			$new_date = '';
		}

		$data = array(
			'list_id' => $this->input->post('list_id'),
			'user_id' => $this->session->userdata('user_id'),
			'title' => $this->input->post('title'),
			'desc' => $this->input->post('desc'),
			'completed' => '0',
			'due_date' => $new_date
			);
		$this->db->set('created_at', 'CURRENT_TIMESTAMP()', FALSE);
		return $this->db->insert($this->TABLE_NAME, $data);

	}

	public function get_task_for_list($id)
	{
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get_where($this->TABLE_NAME, array('list_id'=>$id));
		return $query->result_array();
	}

}
        

?>