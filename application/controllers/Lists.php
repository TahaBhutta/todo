<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lists extends MY_controller {

    function __construct() {
        parent::__construct();
        $this->load->model('list_model');
        $this->load->model('task_model');

        if(!$this->session->userdata('logged_in')){
				redirect('users/login');
      	}
    }

    function index() {
        
        if(!$this->session->userdata('logged_in')){
        	redirect('users/login');
     	}

     	$data['page_title'] = 'My Lists';
     	$data['lists'] = $this->list_model->get_lists();

     	if (empty($data['lists'])) {
        	 $data['lists'] = array();
        }

        $this->load->view('templates/header');
	        $this->load->view('lists/index', $data);
	    $this->load->view('templates/footer');

    }

    public function create()
	{
		if(!$this->session->userdata('logged_in')){
        	redirect('users/login');
     	}

		$data['page_title'] = "Create List";
		
		$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]|max_length[255]');

		if ($this->form_validation->run() === FALSE) {

			$this->load->view('templates/header');
				$this->load->view('lists/create', $data);
			$this->load->view('templates/footer');

		} else {

			if($this->list_model->create()){
				redirect('lists');
			}
			
		}		

	}

	public function tasks($id)
	{	

    	$list = $this->list_model->get_list($id);
    	$data['page_title'] = 'Listing todo\'s from: '.$list['title'];

    	if ($this->session->userdata('user_id') != $list['user_id']) {
    		$this->session->set_flashdata('flash_failure', 'You are not authorized to perform this action');
    		redirect('lists');
    	}

    	$data['list'] = $list;

    	$data['tasks'] = $this->task_model->get_task_for_list($id);

    	$this->load->view('templates/header');
    		$this->load->view('tasks/index', $data);
    	$this->load->view('templates/footer');
	}


}
        

 ?>