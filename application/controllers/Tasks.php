<?php 
	
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Tasks extends MY_controller {
	
	    function __construct() {
	        parent::__construct();
	        $this->load->model('task_model');
	        $this->load->model('list_model');

	        if(!$this->session->userdata('logged_in')){
				redirect('users/login');
      		}

	    }

		//id	list_id	user_id	title	desc	completed	due_date	creator_id

	    function index() {
	    }

	    public function create()
	    {
	    	$data['page_title'] = 'Add New Tasks';
	    	$data['lists'] = $this->list_model->get_lists();

	 		$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[255]');

	 		if ($this->form_validation->run() === FALSE) {
	 			$this->load->view('templates/header');
	 		    	$this->load->view('tasks/create', $data);
	 			$this->load->view('templates/footer');
	 		} else {
	 			if($this->task_model->create()){
	 				redirect($_SERVER['HTTP_REFERER']);
	 			}
	 		}

	    }

	    public function add($list_id)
	    {
	    	//$list_id = $this->input->post('list_id');
	    	// $list = $this->list_model->get_list($list_id);

	    	// if ($this->session->userdata('user_id') != $list['user_id']) {
	    	// 	// Check if user is adding task to his own list
	    	// 	$this->session->set_flashdata('flash_failure', 'You are not authorized to perform this action');
      //   		redirect('lists');
	    	// }
	    }
	
	}
	
 ?>