<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index() {
        
    }

    public function register()
    {
    	$data['page_title'] = 'New Account';

		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[255]|is_unique[users.username]'); 
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]|is_unique[users.email]'); 
 		$this->form_validation->set_rules('password', 'Password', 'required|min_length[3]|max_length[255]');
    	$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[3]|max_length[255]|matches[password]');

    	if ($this->form_validation->run() === FALSE) {
    		$this->load->view('templates/header');
    			$this->load->view('users/register', $data);
    		$this->load->view('templates/footer');
    	} else {
    		
    		//Process Form

    		// Hash Password
    		$hashedPassword = password_hash($this->input->post('password'),PASSWORD_DEFAULT);

    		//Uplaod Image
    		$config['upload_path'] = './assets/images/users';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '2048';
			$config['max_width']  = '1024';
			$config['max_height']  = '1024';

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('profile_img')){
				$errors = array('error' => $this->upload->display_errors());
				$profile_image = 'noimage.jpg';

			}
			else{
				$data = array('upload_data' => $this->upload->data());
				$profile_image = $_FILES['profile_img']['name'];
			}

			if ($this->user_model->register($hashedPassword,$profile_image)) {
				$this->session->set_flashdata('flash_success', 'Profile created');
				redirect('users/register');
			}

    	}

    	
    }

    public function login()
    {
		$data['page_title'] = 'SignIn';

		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[255]'); 
 		$this->form_validation->set_rules('password', 'Password', 'required|min_length[3]|max_length[255]');

 		if ($this->form_validation->run() === FALSE) {
 			$this->load->view('templates/header');
 				$this->load->view('users/login', $data, FALSE);
 			$this->load->view('templates/footer');
 		} else {
 			$username = $this->input->post('username');
 			$password = $this->input->post('password');

 			$user = $this->user_model->login($username);

 			if ($user) {
 				
 				if (password_verify($password, $user['password'])) {
				
					$user_data = array(
						'user_id' => $user['id'],
						'username' => $user['username'],
						'logged_in' => true 
						);

					$this->session->set_userdata( $user_data );

					$this->session->set_flashdata('flash_success', 'You are now logged in');

					redirect('users/login');

				}else{
					$this->session->set_flashdata('flash_failure', 'Username/password missmatch');
					redirect('users/login');
				}

 			}else{
				$this->session->set_flashdata('flash_failure', 'Username/password missmatch');
				redirect('users/login');
			}
 		}

    }

}

?>